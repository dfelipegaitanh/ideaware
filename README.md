**Tabla de Contenidos**

[[_TOC_]]

# Ideaware - Backend test

## Description

La prueba consiste en crear un formulario Web cuya información sea enviada a Aweber usando

[https://api.aweber.com/](https://api.aweber.com/)

- Puedes crear una cuenta gratuita de Aweber en [https://www.aweber.com/free.htm](https://www.aweber.com/free.htm)
- Ten en cuenta los principios, DRY (Don’t repeat yourself), la estructura del código y los archivos, legibilidad del código y mantenibilidad (código y casos de uso pueden ser sujetos a cambios)

**Puntos a desarrollar**
* El formulario debe crear contactos (subscribers) en Aweber, utilizando los campos disponibles.
* Si se ingresa un contacto existente (validación por email), la información en el formulario debe actualizar el contacto en Aweber.
* Se debe aplicar un tag específico en Aweber cuando se cree un contacto nuevo y otro tag cuando se actualiza el contacto 
  * *Tag para contactos nuevos: `test_new_sub`*
  * *Tag para contactos existentes: `test_existing_sub`*
* El formulario debe tener un check de términos y condiciones.
* Debes enviar la IP, fecha, hora y URL desde donde se envió la información del formulario a un custom field en Aweber (Crea el custom field, puedes usar cualquiera nombre). Esta información solo debe ser enviada cuando los términos y condiciones del formulario sean aceptados
* Debes crear un log donde se almacene el envío de la data con el formato: **`CORREO - MM/DD/YY HH:mm:ss`** - *(Agregado exitosamente / Fallo envío {respuesta del servidor} )*
* Favor utilizar PHP (sin framework), orientado a objetos.

## A Considerar

- La solución debe subirse a ***[GitHub](https://github.com/)*** y se debe acceder por medio de un repositorio público.
- Se requiere tener una base de datos para persistencia de la información, adjuntar el dump.
- Favor enviar las credenciales de inicio de sesión de la cuenta de  creada.

Esperamos tus instrucciones para poder armar y correr el programa localmente. En caso de considerarlo necesario añadir instrucciones para realizar un registro exitoso en el formulario.

Puntos extra por hacer ***Unit Testing***

**¡Muchos Éxitos!**

# Desarrollo del proyecto

## Base de datos

En el archivo *database.ini* se encuentra la configuración de la BD. Por favor modifiquela para sus necesidades

```
host = localhost
username = root
password = password
dbname = ideaware
```

## Dump Base de Datos

* ***El archivo de la base de datos esta en [assets/ideaware.sql](assets/ideaware.sql)***
* Los usuarios que estan en la tabla de *users* ya estan creados en [AWeber](#aweber)

## Como instalar

* El equipo donde se instale debe tener 
  * *PHP 7.3.X*
  * [Composer](https://getcomposer.org/). 

* Correr en consola los siguientes comandos
```bash
compoer install
```

* En este punto puede tener el siguiente error

```bash
Problem 1
    - The requested PHP extension ext-json ^1.7 has the wrong version (7.4.8) installed. Install or enable PHP's json extension.
```
* Esto se debe a que la versión de *PHP* que tiene instalada es la *7.4.x*

## Tokens

* Los tokens se actualizan automáticamente.
* Los Tokens se actualizan al correr el test ***ConnectionAPITest::testUpdateAweberTokens***

### Creación de tokens

Correr el comando `php ./helpers/get-access-token` en la opción *Do you wish to create(c) tokens or refresh(r) tokens?* ingresar ***c***

Cuando corra este comando va a tener la siguiente salida
![](assets/get_access_token.png)

La URL que le muestran, ingresarla en el navegador
![](assets/ingresar_imagen.png)

Darle Click a ***Allow Access***
![](assets/nuevo_link.png)

Copiar la URL generada y ponerla en la consola donde dice ***Log in and paste the returned URL here***
![](assets/exito_get_access_token.png)

Si los pasos anteriores fueron exitosos, los [Unit Test](#test) van a correr exitosamente
![](assets/exito_unit_test.png)

### Actualizar tokens 

* **Primera Forma**: Correr el comando `php ./helpers/get-access-token` en la opción *Do you wish to create(c) tokens or refresh(r) tokens?* ingresar ***r***
* **Segunda Forma**: Ingresar en el navegador a la **URL** *http://localhost:8080/get-access-token.php* 
  * *Recuerde cambiar el **http://localhost:8080** por el host que este manejando*

## Primero Pasos

* Correr todos los [Unit Test](#test) en especial [testCanConnectToTheApi](#connectionapi) para validar que se puede conectara al API
* Si falla los [Unit Test](#test) ir a [*tokens*](#tokens) para refrescar los tokens y así la aplicación pueda funcionar
* La *URL* del proyecto debe ser *http://localhost:8080/*, de no ser así se deberia modificar en el *OAuth Redirect URL* y el *Application Website* dentro del aplicativo

![](./assets/aweber_app.png)

También se debe modificar el archivo *helpers/get-access-token* en la línea *19* y poner la ***URL*** con la cual se va a trabajar

### Opcionales

* Si desea crear datos *"falsos"* tanto en la Base de datos como en [AWeber](#aweber) ingrese a la siguiente **URL** *http://localhost:8080/fakeData.php* 
  * *Recuerde cambiar el **http://localhost:8080** por el host que este manejando*

## [AWeber](https://www.aweber.com/)

### Credenciales

* **Email** `felipegaitan81@gmail.com`
* **Password** `FelipeIdeaware@`

## [AWeber API](https://api.aweber.com/)
 
### Credenciales

* **Email:** `dfelipegaitanh@gmail.com`
* **Password:** `FelipeIdeaware@`
* **Client ID:** `4xGW2VDK9GJ5PHfh3JzxRd6YO8dGzDus`
* **Client Secret:** `KbuX3zn7O2L8iOlWnJspdKtiEyJUprGO`
* **redirect_uri:** `http://localhost:8080/`

## Unit Test

### ConnectionAPI

* **testCanConnectToTheApi**: *Valida si se puede conectar al API*
* **testGetAccountsCollection**: *Validar que getAccountsCollection no llegue vacio*
* **testUpdateAweberTokens**: *Valida si el token se acualiza correctamente*

### ConnectionDBTest

* **testConnect**: *Valida la conexión a la Base de datos*
  * Si le sale el siguiente mensaje `PDOException: PDO::__construct(): The server requested authentication method unknown to the client [caching_sha2_password]` correr el siguiente comando en la Base de Datos **con sus respectivas credeciales**
```sql
SET GLOBAL validate_password.policy=LOW;
ALTER USER 'root'@'localhost' IDENTIFIED WITH mysql_native_password BY 'password';
flush privileges;
```
* **testAgregarLog**: *Validar una entrada exitosa a la BD*
* **testAgregarLogError**: *Validar una entrada NO exitosa a la BD*

### AccountTest

* **testGetSubscribersCollectionLink**: *Test Para Obtener Subscriber Link*
* **testCreateSubscriber**: *Test Para Crear un Subscriber*
* **testGetAccounts**: *Test Para Obtener Subscribers (No debe ser vacio)*
* **testUpdateSubscriber**: *Test Para Actualizar un Subscriber ya existente*

### CustomFieldTest

* **testGetCustomFields**: *Validar si existen Custom Fields*
  * Si obtiene el siguiente error `Failed asserting that an array is not empty.` Por favor cree los *Custom Fields* ***host, ip_address, fecha, hora, url***
