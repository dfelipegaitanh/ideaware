-- MySQL dump 10.13  Distrib 8.0.21, for osx10.15 (x86_64)
--
-- Host: 127.0.0.1    Database: ideaware
-- ------------------------------------------------------
-- Server version	8.0.21

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `user` (
  `id` int NOT NULL AUTO_INCREMENT,
  `email` varchar(50) NOT NULL DEFAULT '',
  `fecha` datetime NOT NULL,
  `mensaje` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (1,'christy57@yahoo.com','2020-07-27 04:43:02','Usuario Creado (Unit Test)'),(2,'christy57@yahoo.com','2020-07-27 04:43:04','Usuario Actualizado (Unit Test)'),(3,'nedra53@hotmail.com','2009-01-25 00:00:00','beatae sunt (Unit Test)'),(4,'nathen32@hotmail.com','2020-07-27 04:47:16','Usuario Creado (Unit Test)'),(5,'christy57@yahoo.com','2020-07-27 04:47:18','Usuario Actualizado (Unit Test)'),(6,'isidro88@yahoo.com','1983-03-06 00:00:00','molestiae id (Unit Test)'),(7,'pemmerich@yahoo.com','2020-07-27 04:47:46','Agregado exitosamente'),(8,'jakayla.hintz@veum.com','2020-07-27 04:47:47','Agregado exitosamente'),(9,'elva.hilpert@quigley.com','2020-07-27 04:47:48','Agregado exitosamente'),(10,'lesch.garrison@wolff.com','2020-07-27 04:47:49','Agregado exitosamente'),(11,'howell.lorena@gmail.com','2020-07-27 04:47:50','Agregado exitosamente'),(12,'gustave88@gmail.com','2020-07-27 04:47:51','Agregado exitosamente'),(13,'yost.derick@yahoo.com','2020-07-27 04:47:51','Agregado exitosamente'),(14,'ritchie.johnson@yahoo.com','2020-07-27 04:47:52','Agregado exitosamente'),(15,'cgoyette@hotmail.com','2020-07-27 04:47:53','Agregado exitosamente'),(16,'dalton82@cummerata.com','2020-07-27 04:47:54','Agregado exitosamente'),(17,'jose.greenholt@gmail.com','2020-07-27 04:47:55','Agregado exitosamente'),(18,'darlene75@mante.org','2020-07-27 04:47:56','Agregado exitosamente'),(19,'emerald56@gmail.com','2020-07-27 04:47:57','Agregado exitosamente'),(20,'llubowitz@kovacek.biz','2020-07-27 04:47:58','Agregado exitosamente'),(21,'jocelyn.hill@lubowitz.com','2020-07-27 04:47:59','Agregado exitosamente'),(22,'harber.tomas@bashirian.biz','2020-07-27 04:48:00','Agregado exitosamente'),(23,'hayes.deja@thompson.com','2020-07-27 04:48:01','Agregado exitosamente'),(24,'kane.rempel@yahoo.com','2020-07-27 04:48:02','Agregado exitosamente'),(25,'juwan32@hotmail.com','2020-07-27 04:48:03','Agregado exitosamente'),(26,'henri.bogisich@collier.net','2020-07-27 04:48:04','Agregado exitosamente');
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-07-26 23:48:47
