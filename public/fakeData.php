<?php

use GuzzleHttp\Exception\GuzzleException;
use Ideaware\Aweber\Account;
use Ideaware\Aweber\FunctionsTrait;

require __DIR__ . '/../vendor/autoload.php';

$account = new Account();

$numeroSubscriber = 20;
for ($i = 0; $i < $numeroSubscriber; $i++) {
    $data            = FunctionsTrait::getAccountData([], true);
    $subscribersLink = $account->_getSubscribersCollectionLink();
    try {
        $account->_addSubscriber($data, $subscribersLink);
    } catch (GuzzleException $e) {
    }
}