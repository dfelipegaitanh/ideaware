<?php

session_start();

use Ideaware\Form\Form;
use Ideaware\Html\Helper;

require __DIR__ . '/../vendor/autoload.php';

?>

<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">

    <title>Ideaware Test</title>
    <meta name="description" content="Ideaware Test">
    <meta name="author" content="Felipe Gaitan">

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
    <link href="./css/styles.css" rel="stylesheet">

</head>

<body>

    <body data-gr-c-s-loaded="true">
        <div class="container-fluid">
            <?php Helper::renderMensaje(); ?>
            <div class="row">
                <form class="form-signin" autocomplete="off" method="POST" action="addSubscriber.php">
                    <?php Form::render() ?>
                </form>
            </div>
    </body>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>
</body>

</html>