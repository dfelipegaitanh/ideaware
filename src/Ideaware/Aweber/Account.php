<?php


namespace Ideaware\Aweber;


use Exception;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Exception\ServerException;
use Ideaware\Connection\ConnectionAPI;

class Account extends ConnectionAPI
{

    use FunctionsTrait;

    /**
     * @param array $params
     * @return string
     */
    public function _getSubscribersCollectionLink( array $params = [] ): string
    {
        return $this->getSubscribersCollectionLink($params);
    }

    /**
     * @param array $params
     * @return string
     */
    private function getSubscribersCollectionLink( array $params = [] ): string
    {
        $accounts = $this->getAccountsCollection();
        $lists    = $this->getCollection($accounts['lists_collection_link']);
        return is_null($lists) ? '' : $lists[0]['subscribers_collection_link'] . ( !empty($params) ? '?' . http_build_query($params) : '' );
    }

    /**
     * @param array $params
     * @throws GuzzleException
     */
    public function manageAccount( array $params )
    {
        $data = self::getAccountData($params);
        list($account, $subscribersLink) = $this->findSubscriber($params['email']);

        empty($account) ?
            $this->addSubscriber($data, $subscribersLink) :
            $this->updateExistingSubscriber($data, $account[0]['self_link']);


        header("Location: " . $this->getHostURL());
    }

    /**
     * @param string $email
     * @return array
     */
    public function findSubscriber( string $email ): array
    {
        $params = ['ws.op' => 'find', 'email' => $email];
        return [
            $this->getCollection($this->getSubscribersCollectionLink($params)),
            $this->getSubscribersCollectionLink()
        ];
    }

    /**
     * @param $data
     * @param $subscribersLink
     * @throws GuzzleException
     */
    private function addSubscriber( $data, $subscribersLink ): void
    {
        try {
            $mensajeDB = "Agregado exitosamente";
            $this->createSubscriber($data, $subscribersLink);
            $clase = "success";
        } catch (ClientException $e) {
            list($mensajeDB, $clase) = $this->getMessagesDBAndHTML($e);
        }
        self::agregarLog($_REQUEST['email'] ?? $data['email'], $mensajeDB);
        $texto = ( $clase === "success" ) ? "El usuario se ha creado exitosamente" : "El usuario no se ha podido crear";

        $_SESSION["mensaje"] = [
            "clase" => $clase,
            "texto" => $texto
        ];
    }

    /**
     * @param $data
     * @param $url
     * @return mixed
     * @throws GuzzleException
     */
    private function createSubscriber( $data, $url )
    {
        $subscriberUrl = $this->postUrlApi($url, $data)
                              ->getHeader('Location')[0];
        return $this->getUrlApi($subscriberUrl, $data);
    }

    /**
     * @param $e
     * @return string[]
     */
    private function getMessagesDBAndHTML( $e ): array
    {
        $mensajeException = json_encode($e->getMessage());
        $mensajeDB        = "Fallo envío: '{$mensajeException}'";
        $clase            = "danger";
        return array($mensajeDB, $clase);
    }

    /**
     * @param array $data
     * @param $self_link
     * @throws GuzzleException
     */
    private function updateExistingSubscriber( array $data, $self_link ): void
    {
        try {
            $mensajeDB = "Actualizado exitosamente";
            $this->updateSubscriber($data, $self_link);
            $clase = "success";
        } catch (ServerException $e) {
            list($mensajeDB, $clase) = $this->getMessagesDBAndHTML($e);
        } catch (ClientException $e) {
            list($mensajeDB, $clase) = $this->getMessagesDBAndHTML($e);
        }
        self::agregarLog($_REQUEST['email'] ?? $data['email'], $mensajeDB);
        $texto = ( $clase === "success" ) ? "El usuario se ha actualizado exitosamente" : "El usuario no se ha podido actualizar";

        $_SESSION["mensaje"] = [
            "clase" => $clase,
            "texto" => $texto
        ];
    }

    /**
     * @param $data
     * @param $url
     * @return mixed
     * @throws GuzzleException
     */
    private function updateSubscriber( $data, $url )
    {
        self::modifyTagsUpdateAccount($data);
        return $this->patchUrlApi($url, $data);
    }

    /**
     * @param $data
     * @param $url
     * @return mixed
     * @throws GuzzleException
     */
    public function _updateSubscriber( $data, $url = "" )
    {
        return $this->updateSubscriber($data, $url);
    }

    /**
     * @deprecated
     */
    public function deleteSubscribers()
    {
        foreach ($this->getAccountsEmails() as $email) {
            $this->deleteSubscriber($email);
        }
    }

    /**
     * @return array
     * @deprecated
     */
    public function getAccountsEmails(): array
    {
        $emails = [];
        foreach ($this->getAccounts() as $account) {
            $emails[] = $account['email'];
        }
        return $emails;
    }

    /**
     * @return array|Exception|GuzzleException
     */
    public function getAccounts()
    {
        $params = ['ws.op' => 'find'];
        return $this->getCollection($this->getSubscribersCollectionLink($params));
    }

    /**
     * @param $email
     * @deprecated
     */
    private function deleteSubscriber( $email )
    {
        list($data, $url) = $this->findSubscriber($email);
        $url .= "?" . http_build_query(["subscriber_email" => $email]);
        try {
            $this->deleteUrlApi($url);
        } catch (GuzzleException $e) {
        }
    }

    /**
     * @param $data
     * @param $url
     * @return mixed
     * @throws GuzzleException
     */
    public function _createSubscriber( $data, $url )
    {
        return $this->createSubscriber($data, $url);
    }

    /**
     * @param $data
     * @param $subscribersLink
     * @throws GuzzleException
     */
    function _addSubscriber( $data, $subscribersLink ): void
    {
        $this->addSubscriber($data, $subscribersLink);
    }
}
