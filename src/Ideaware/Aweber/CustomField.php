<?php


namespace Ideaware\Aweber;


use Ideaware\Connection\ConnectionAPI;

class CustomField extends ConnectionAPI
{

    public function getCustomFields()
    {
        return $this->getCollection($this->getCustomFieldUrl());
    }

    private function getCustomFieldUrl()
    {
        $account = $this->getAccountsCollection();
        $lists = $this->getCollection($account['lists_collection_link']);
        return is_null($lists) ? '' : $lists[0]['custom_fields_collection_link'];
    }

}