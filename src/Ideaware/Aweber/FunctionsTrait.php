<?php


namespace Ideaware\Aweber;


use Faker\Factory;
use Ideaware\Connection\ConnectionDB;

trait FunctionsTrait
{

    /**
     * @param array $params
     * @param bool $fakerData
     * @return array
     */
    static function getAccountData( array $params = [], bool $fakerData = false ): array
    {
        $faker = Factory::create();

        $name        = self::withFaker($fakerData) ? $faker->name : ( $params['name'] ?? '' );
        $email       = self::withFaker($fakerData) ? $faker->email : ( $params['email'] ?? '' );
        $fecha       = self::withFaker($fakerData) ? $faker->date() : date('yy-m-d');
        $hora        = self::withFaker($fakerData) ? $faker->time() : date('H:i:s');
        $url         = self::withFaker($fakerData) ? $faker->url : self::getActualURL();
        $host        = self::withFaker($fakerData) ? $faker->domainName : self::getDomainName();
        $ip_address  = self::withFaker($fakerData) ? $faker->ipv4 : $_SERVER['SERVER_ADDR'];
        $ad_tracking = self::withFaker($fakerData) ? $faker->text(40) : ( $params['adTracking'] ?? '' );
        $tags        = ['test_new_sub'];

        return [
            "name"          => $name,
            "email"         => $email,
            "ad_tracking"   => $ad_tracking,
            'tags'          => $tags,
            'status'        => 'subscribed',
            'custom_fields' => [
                'host'       => $host,
                'ip_address' => $ip_address,
                'fecha'      => $fecha,
                'hora'       => $hora,
                'url'        => $url
            ]
        ];
    }

    /**
     * @param bool $fakerData
     * @return bool
     */
    static function withFaker( bool $fakerData ): bool
    {
        return $fakerData === true;
    }

    /**
     * @return string
     */
    static function getActualURL(): string
    {
        return ( isset($_SERVER['HTTPS']) && $_SERVER['HTTPS']
            === 'on' ? "https" : "http" ) . "://" .
            $_SERVER['HTTP_HOST'] . $_SERVER['PHP_SELF'];
    }

    /**
     * @return string
     */
    static private function getDomainName(): string
    {
        return getHostByName(getHostName());
    }

    static function modifyTagsUpdateAccount( &$data )
    {
        $data["tags"] = [
            'add'    => [
                'test_existing_sub'
            ],
            'remove' => [
                'test_new_sub'
            ]
        ];
        unset($data['status']);
    }

    /**
     * @param $email
     * @param $mensaje
     * @return bool|false|int
     */
    static function agregarLog( $email, $mensaje )
    {
        $fecha = date('yy-m-d H:i:s');
        $query = "INSERT INTO user ( email , fecha, mensaje ) VALUES ( '{$email}' , '{$fecha}', '{$mensaje}' )";
        return ( new ConnectionDB() )->exec($query);
    }

    /**
     * @return string
     */
    function getHostURL(): string
    {
        return ( isset($_SERVER['HTTPS']) && $_SERVER['HTTPS']
            === 'on' ? "https" : "http" ) . "://" .
            $_SERVER['HTTP_HOST'];
    }
}