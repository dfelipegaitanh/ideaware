<?php


namespace Ideaware\Connection;


use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;

trait AweberTokenTrait
{

    /**
     * @throws GuzzleException
     */
    public static function updateAweberTokens()
    {

        $tokenUrl    = 'https://auth.aweber.com/oauth2/token';
        $credentials = parse_ini_file(__DIR__ . '/../../../credentials.ini');
        try {
            $newCredentials = self::getNewCredentials($tokenUrl, $credentials);
        } catch (GuzzleException $e) {
            echo "Problems at updating the tokens. Please do it manually";
            throw $e;
        }
        self::updateCredentialsFile($credentials, $newCredentials);
    }

    /**
     * @param $tokenUrl
     * @param $credentials
     * @return array
     * @throws GuzzleException
     */
    private static function getNewCredentials( $tokenUrl, $credentials )
    {
        try {
            return json_decode(
                ( new Client() )->post($tokenUrl, self::getOptionsPost($credentials))
                                ->getBody(), true);
        } catch (GuzzleException $e) {
            throw $e;
        }
    }

    /**
     * @param array $credentials
     * @return array[]
     */
    private static function getOptionsPost( $credentials ): array
    {
        return [
            'auth' => [
                $credentials['clientId'],
                $credentials['clientSecret']
            ],
            'json' => [
                'grant_type'    => 'refresh_token',
                'refresh_token' => $credentials['refreshToken']
            ]
        ];
    }

    /**
     * @param array $credentials
     * @param array $newCredentials
     */
    private static function updateCredentialsFile( array $credentials, array $newCredentials ): void
    {
        $fp = fopen(__DIR__ . '/../../../credentials.ini', 'wt');
        fwrite(
            $fp,
            "clientId = {$credentials['clientId']}
clientSecret = {$credentials['clientSecret']}
accessToken = {$newCredentials['access_token']}
refreshToken = {$newCredentials['refresh_token']}"
        );
        fclose($fp);
        chmod(__DIR__ . '/../../../credentials.ini', 0600);
    }

}
