<?php

namespace Ideaware\Connection;

use Exception;
use GuzzleHttp\Client as ClientAweber;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\GuzzleException;
use Psr\Http\Message\ResponseInterface;

class ConnectionAPI
{

    use AweberTokenTrait;

    /**
     * @var string
     */
    public $BASE_URI;
    /**
     * @var array
     */
    private $credentials;
    /**
     * @var ClientAweber
     */
    private $client;
    /**
     * @var string
     */
    private $TOKEN_URL;
    /**
     * @var string
     */
    private $host;

    function __construct()
    {
        $this->TOKEN_URL   = 'https://auth.aweber.com/oauth2/token';
        $this->BASE_URI    = 'https://api.aweber.com/1.0/';
        $this->readCredentialFiles();
        $this->client      = new ClientAweber();
    }

    /**
     * @return array
     */
    public function getAccountsCollection(): array
    {
        $accounts = $this->getCollection($this->BASE_URI . 'accounts');
        return empty($accounts) ? [] : $accounts[0];
    }

    /**
     * @param $url
     * @return array|Exception|GuzzleException
     */
    public function getCollection( $url )
    {
        try {
            $collection = [];
            while (isset($url) and !empty($url)) {
                try {
                    $request    = $this->client->get(
                        $url,
                        ['headers' => ['Authorization' => 'Bearer ' . $this->credentials['accessToken']]]
                    );
                    $body       = $request->getBody();
                    $page       = json_decode($body, true);
                    $collection = array_merge($page['entries'], $collection);
                    $url        = isset($page['next_collection_link']) ? $page['next_collection_link'] : null;
                } catch (ClientException $e) { // Update the tokens
                    $this->updateAweberTokens();
                    $this->readCredentialFiles();
                }
            }
            return $collection;
        } catch (GuzzleException $e) {
            return $e;
        }
    }

    /**
     * @param $url
     * @param $data
     * @return mixed
     * @throws GuzzleException
     */
    public function patchUrlApi( $url, $data )
    {
        return json_decode($this->client->patch($url, [
            'json'    => $data,
            'headers' => ['Authorization' => 'Bearer ' . $this->credentials['accessToken']]
        ])
                                        ->getBody(), true);
    }

    /**
     * @param $url
     * @param $data
     * @return ResponseInterface
     * @throws GuzzleException
     */
    public function postUrlApi( $url, $data ): ResponseInterface
    {
        return $this->client->post($url, [
            'json'    => $data,
            'headers' => ['Authorization' => 'Bearer ' . $this->credentials['accessToken']]
        ]);
    }

    /**
     * @param $url
     * @return ResponseInterface
     * @throws GuzzleException
     */
    public function deleteUrlApi( $url ): ResponseInterface
    {
        return $this->client->delete($url, [
            'headers' => ['Authorization' => 'Bearer ' . $this->credentials['accessToken']]
        ]);
    }

    /**
     * @param $url
     * @param $data
     * @return mixed
     * @throws GuzzleException
     */
    public function getUrlApi( $url, $data )
    {
        return json_decode($this->client->get($url, [
            'json'    => $data,
            'headers' => ['Authorization' => 'Bearer ' . $this->credentials['accessToken']]
        ])->getBody(), true);
    }

    private function readCredentialFiles(): void
    {
        $this->credentials = parse_ini_file(__DIR__ . '/../../../credentials.ini');
    }
}
