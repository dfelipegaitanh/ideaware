<?php


namespace Ideaware\Form;

class Field
{

    public static function renderField($field)
    {

        if (isset($field['is_subscriber_updateable']) and $field['is_subscriber_updateable'] === true) {
            return;
        }

        $required = $field['required'] ?? '';
        $type = $field['type'] ?? 'text';
        $name = $field['name'] ?? '';
        $label = $field['label'] ?? $field['name'];
        $id = $field['id'] ?? '';
        $class = $field['class'] ?? 'form-control';
        $maxlength = isset($field['maxlength']) ? "maxlength='{$field['maxlength']}'" : '';
        $placeholder = "placeholder='" . !isset($field['placeholder']) ? "id{$field['name']}" : $field['placeholder'] . "'";

        echo "
        <div class='form-label-group'>
            <input type='{$type}' name='{$name}' id='{$id}' class='{$class}' {$placeholder} {$required} {$maxlength}>
            <label for='{$id}'>{$label}</label>
        </div>
        ";
    }

    public static function renderButton($field)
    {
        echo "<button class='{$field['class']}' type='{$field['type']}'>{$field['text']}</button> ";
    }

}