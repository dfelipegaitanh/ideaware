<?php


namespace Ideaware\Form;

class Form
{

    /**
     * @var array[]
     */
    public static $fields = [
        "fields"             => [
            "name"       => [
                "name"        => "name",
                "id"          => "inputName",
                "type"        => "text",
                "placeholder" => "Name",
                "label"       => "Name",
                "class"       => "form-control",
                "required"    => "required",
                "maxlength"   => 60
            ],
            "email"      => [
                "name"        => "email",
                "id"          => "inputEmail",
                "type"        => "email",
                "placeholder" => "Email address",
                "label"       => "Email address",
                "class"       => "form-control",
                "required"    => "required",
                "maxlength"   => 50
            ],
            "adTracking" => [
                "name"        => "adTracking",
                "id"          => "inputTracking",
                "type"        => "text",
                "placeholder" => "Ad Tracking",
                "label"       => "Ad Tracking",
                "class"       => "form-control",
                "required"    => "required",
                "maxlength"   => 20
            ]
        ],
        "buttons"            => [
            "addSubscriber" => [
                "class" => "btn btn-lg btn-primary btn-block",
                "type"  => "submit",
                "text"  => "Add Subscriber"
            ]
        ],
        "termsAndConditions" => [
            "name"        => "termsAndConditions",
            "type"        => "checkbox",
            "placeholder" => "Terminos y condiciones",
            "label"       => "Terminos y condiciones",
            "class"       => "form-control terms-and-conditions",
            "required"    => "required"
        ]
    ];

    public static function render()
    {
        foreach (self::$fields['fields'] as $field) {
            Field::renderField($field);
        }
        /*
        foreach ((new CustomField())->getCustomFields() as $field) {
            Field::renderField($field);
        }
        */
        foreach (self::$fields['buttons'] as $field) {
            Field::renderButton($field);
        }
        Field::renderField(self::$fields['termsAndConditions']);
    }
}
