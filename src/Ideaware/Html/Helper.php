<?php


namespace Ideaware\Html;


class Helper
{

    public static function renderMensaje()
    {
        if (isset($_SESSION['mensaje']) and !empty($_SESSION['mensaje'])) {
            $clase = $_SESSION['mensaje']['clase'] ?? 'primary';
            $texto = $_SESSION['mensaje']['texto'] ?? '';
            echo "
            <div class='row'>
                <div class='alert alert-{$clase} mx-auto' role='alert'>{$texto}</div>
            </div>
            ";
            session_unset();
        }
    }
}
