<?php

namespace Tests\Aweber;

use GuzzleHttp\Exception\GuzzleException;
use Ideaware\Aweber\Account;
use Ideaware\Aweber\FunctionsTrait;
use PHPUnit\Framework\TestCase;

class AccountTest extends TestCase
{

    /**
     * Test Para Obtener Subscriber Link
     */
    public function testGetSubscribersCollectionLink()
    {
        $this->assertNotEmpty(( new Account() )->_getSubscribersCollectionLink());
    }

    /**
     * Test Para Crear un Subscriber
     * @throws GuzzleException
     */
    public function testCreateSubscriber()
    {
        $subscribersLink = ( new Account() )->_getSubscribersCollectionLink();
        $data            = FunctionsTrait::getAccountData([], true);
        $account         = ( new Account() )->_createSubscriber($data, $subscribersLink);
        $this->assertNotEmpty($account);

        $this->assertTrue(FunctionsTrait::agregarLog($data['email'], "Usuario Creado (Unit Test)"));
    }

    /**
     * Test Para Actualizar un Subscriber ya existente
     * @throws GuzzleException
     */
    public function testUpdateSubscriber()
    {
        $accountClass = new Account();

        $accounts = $accountClass->getAccounts();
        $this->assertNotEmpty($accounts);

        $account = $accounts[array_rand($accounts)];
        $this->assertNotEmpty($account);

        $data          = FunctionsTrait::getAccountData([], true);
        $data['email'] = $account['email'];
        FunctionsTrait::modifyTagsUpdateAccount($data);

        list($account, $subscribersLink) = $accountClass->findSubscriber($data['email']);
        $account = $accountClass->_updateSubscriber($data, $account[0]['self_link']);
        $this->assertNotEmpty($account);

        $this->assertTrue(FunctionsTrait::agregarLog($data['email'], "Usuario Actualizado (Unit Test)"));

    }

    /**
     * Test Para Obtener Subscribers
     */
    public function testGetAccounts()
    {
        $accounts = ( new Account() )->getAccounts();
        $this->assertNotEmpty($accounts);
    }

}
