<?php

namespace Tests\Aweber;

use Ideaware\Aweber\CustomField;
use PHPUnit\Framework\TestCase;

class CustomFieldTest extends TestCase
{

    /**
     * Validar si existen Custom Fields
     */
    public function testGetCustomFields()
    {
        $customFields = (new CustomField())->getCustomFields();
        $this->assertNotEmpty($customFields);
    }
}
