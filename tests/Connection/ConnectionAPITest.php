<?php

namespace Tests\Connection;

use GuzzleHttp\Exception\GuzzleException;
use Ideaware\Connection\AweberTokenTrait;
use Ideaware\Connection\ConnectionAPI;
use PHPUnit\Framework\TestCase;

class ConnectionAPITest extends TestCase
{

    /**
     * Valida si se puede conectar al API
     */
    public function testCanConnectToTheApi()
    {
        $this->assertNotInstanceOf(GuzzleException::class, (new ConnectionAPI())->getCollection((new ConnectionAPI())->BASE_URI . 'accounts'));
    }

    /**
     * Validar que getAccountsCollection no llegue vacio
     */
    public function testGetAccountsCollection(){
        $this->assertNotEmpty((new ConnectionAPI())->getAccountsCollection());
    }

    /**
     * Valida si el token se acualiza correctamente
     */
    public function testUpdateAweberTokens()
    {
        $this->assertNotInstanceOf(GuzzleException::class, AweberTokenTrait::updateAweberTokens());
    }

}
