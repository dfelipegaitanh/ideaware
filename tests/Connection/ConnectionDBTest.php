<?php

namespace Tests\Connection;

use Faker\Factory;
use Ideaware\Connection\ConnectionDB;
use PDOException;
use PHPUnit\Framework\TestCase;

class ConnectionDBTest extends TestCase
{

    /**
     * Valida Conexión a Base de Datos
     */
    public function testConnect()
    {
        $this->assertNotInstanceOf(PDOException::class, new ConnectionDB());
    }

    /**
     * Validar una entrada exitosa a la BD
     */
    public function testAgregarLog()
    {
        $faker = Factory::create();

        $email   = $faker->email;
        $fecha   = $faker->date();
        $mensaje = $faker->words(2, true) . " (Unit Test)";
        $query   = "INSERT INTO user ( email , fecha, mensaje ) VALUES ( '{$email}' , '{$fecha}', '{$mensaje}' )";
        $this->assertTrue(( new ConnectionDB() )->exec($query));
    }

    /**
     * Validar una entrada NO exitosa a la BD
     */
    public function testAgregarLogError(){
        $query   = "INSERT INTO user ( email , fecha, mensaje ) VALUES ( '' , '', '' )";
        $this->assertFalse(( new ConnectionDB() )->exec($query));
    }
}
